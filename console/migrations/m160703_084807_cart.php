<?php

use yii\db\Migration;

class m160703_084807_cart extends Migration
{
    public function up()
    {
        $this->createTable('Cart',[
           'cartId' => 'pk',
           'userId' => 'int(11)',
           'total' => 'decimal',
        ]);
        $this->createTable('CartItem',[
           'cartItemId' => 'pk',
           'cartId' => 'int(11)',
           'productId' => 'int(11)',
//           'price' => 'decimal',
        ]);


        $this->addForeignKey('Cart_User_fk','Cart','userId',
            '{{%user}}','id');

        $this->addForeignKey('CartItem_Cart_fk','CartItem','cartId',
            'Cart','cartId');

        $this->addForeignKey('CartItem_Product_fk','CartItem','productId',
            'Product','productId');
    }

    public function down()
    {
        $this->dropForeignKey('Cart_User_fk','Cart');
        $this->dropForeignKey('CartItem_Cart_fk','CartItem');
        $this->dropForeignKey('CartItem_Product_fk','CartItem');

        /**
         * SET foreign_key_checks = 0;
         * .....
         * удаляеться без проверки
         * .....
         * SET foreign_key_checks = 1;
         */

        $this->dropTable('CartItem');
        $this->dropTable('Cart');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
