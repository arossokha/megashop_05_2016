<?php

use yii\db\Migration;

class m160703_062756_updateUser extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}','profile','text');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}','profile');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
