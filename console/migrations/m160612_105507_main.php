<?php

use yii\db\Migration;

class m160612_105507_main extends Migration
{
    public function up()
    {
        $this->createTable('Category',[
            'categoryId' => 'pk',
            'name' => 'char(50)',
        ],'ENGINE=InnoDB');

        $this->createTable('Brand',[
            'brandId' => 'pk',
            'name' => 'string(50)',
        ],'ENGINE=InnoDB');

        $this->createTable('Product',[
            'productId' => 'pk',
            'name' => 'string(50)',
            'price' => 'decimal',
            'photo' => 'varchar(500)',
            'description' => 'text',
            'categoryId' => 'integer',
            'brandId' => 'integer',
        ]);

        $this->addForeignKey('Product_Category_fk','Product','categoryId',
            'Category','categoryId');

        $this->addForeignKey('Product_Brand_fk','Product','brandId',
            'Brand','brandId');

    }

    public function down()
    {
        $this->dropForeignKey('Product_Category_fk','Product');
        $this->dropForeignKey('Product_Brand_fk','Product');
        $this->dropTable('Product');
        $this->dropTable('Category');
        $this->dropTable('Brand');
    }

}
