<?php

use yii\db\Migration;

class m160626_092357_updateProduct extends Migration
{
    public function up()
    {
        /**
         * @todo: в каждой моделе существующей задать слаг
         */
        $this->addColumn('Product','slug','string(50)');
        $this->createIndex('unique_product_slug','Product','slug',true);
    }

    public function down()
    {
        $this->dropColumn('Product','slug');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
