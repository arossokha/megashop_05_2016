<?php

use yii\db\Migration;

class m160618_092605_photo extends Migration
{
    public function up()
    {
        /**
         * @todo HOMEWORK: сохраоняем для каждого впродукта путь к его фотографии
         *
         * созать файл в папке runtime
         * складываем информацию содержащюю
         *   путь фото
         *   ид продукта
         *   имя продукта
         * тянем эту информацию из базы данных из таблицы Продукта
         *
         */
        $this->dropColumn('Product','photo');
        $this->addColumn('Product','photoId','integer(11)');


        $this->createTable('Photo',[
            'photoId' => 'pk',
            'name' => 'string(50)',
            'path' => 'string(500)',
            'productId' => 'int(11)',
            // почитать детали в документе (там есть
            // фото с разбором структуры)
            // 'modelId' => 'int(11)',
            // 'modelType' => 'enum("product","user")' || "string(50)"
        ],'ENGINE=InnoDB');

        $this->addForeignKey('Product_Photo_fk','Product','photoId',
            'Photo','photoId');

        $this->addForeignKey('Photo_Product_fk','Photo','photoId',
            'Product','photoId');

        /**
         * @todo HOMEWORK: из сохраненных данных о фотографиях
         * @todo HOMEWORK: заполяняем таблицу photo
         * @todo HOMEWORK: На сонове имени продукта и пути к фото
         * @todo HOMEWORK: В product заполняем photoId
         *
         * читаете файл
         * созадаёте записи в БД только уже для таблицы Photo
         * вставляем полученые ид фото в таблицу Продукта
         */

    }

    public function down()
    {
        /**
         * @todo HOMEWORK: вернуть данные назад
         */
        $this->dropForeignKey('Product_Photo_fk','Product');
        $this->dropForeignKey('Photo_Product_fk','Photo');
        $this->dropTable('Photo');

        $this->dropColumn('Product','photoId');
        $this->addColumn('Product','photo','string(500)');

        /**
         * @todo HOMEWORK: вернуть данные назад
         */

    }

}
