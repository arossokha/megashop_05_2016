<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * This command for some important thing
 */
class ImportantController extends Controller
{
    /**
     * Important command
     */
    public function actionIndex($name = 'man')
    {
        $name = $this->ansiFormat($name, Console::FG_YELLOW);

        $name = Yii::getAlias('@runtime');
        $str = "Hello {$name}\n";

        $this->stdout($str, Console::BOLD);
    }
}