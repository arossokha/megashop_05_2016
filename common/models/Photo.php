<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Photo".
 *
 * @property integer $photoId
 * @property string $name
 * @property string $path
 * @property integer $productId
 *
 * @property Product $photo
 * @property Product[] $products
 */
class Photo extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['path'], 'string', 'max' => 500],
            [['photoId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['photoId' => 'photoId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'photoId' => Yii::t('app', 'Photo ID'),
            'name' => Yii::t('app', 'Name'),
            'path' => Yii::t('app', 'Path'),
            'productId' => Yii::t('app', 'Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(Product::className(), ['photoId' => 'photoId'])->inverseOf('photo');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['photoId' => 'photoId'])->inverseOf('photo0');
    }

    /**
     * @inheritdoc
     * @return PhotoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PhotoQuery(get_called_class());
    }
}
