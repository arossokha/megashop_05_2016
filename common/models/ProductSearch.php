<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @var string
     */
    public $search;

    /**
     * @var array
     */
    public $brands;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', 'categoryId', 'brandId', 'photoId'], 'integer'],
            [['name', 'description'], 'safe'],
            [['search','brands'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $searchParams
     *
     * @return ActiveDataProvider
     */
    public function search($searchParams)
    {
        $query = Product::find();

        $query->joinWith('category');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5
            ]
        ]);

        $this->load($searchParams);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'productId' => $this->productId,
            'price' => $this->price,
            'categoryId' => $this->categoryId,
            'brandId' => $this->brandId,
            'photoId' => $this->photoId,
        ]);

        $query->andFilterWhere(['like', '`'.Product::tableName().'`.name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        $query->orFilterWhere(['like', '`'.Product::tableName().'`.name', $this->search])
            ->orFilterWhere(['like', 'description', $this->search])
            ->orFilterWhere(['=','price', $this->search])
            ->orFilterWhere(['like', '`'.Category::tableName().'`.name', $this->search]);

        $query->andFilterWhere([
            'in', '`'.Product::tableName().'`.brandId', $this->brands]
        );

        return $dataProvider;
    }
}
