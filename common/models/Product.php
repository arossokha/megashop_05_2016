<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "Product".
 *
 * @property integer $productId
 * @property string $name
 * @property string $price
 * @property string $description
 * @property integer $categoryId
 * @property integer $brandId
 * @property integer $photoId
 * @property string $slug
 *
 * @property Photo $photo
 * @property Brand $brand
 * @property Category $category
 * @property Photo $photo0
 */
class Product extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'brandId','price','name','slug'], 'required'],
            [['price'], 'number'],
            [['description'], 'string'],
            [['categoryId', 'brandId', 'photoId'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name', 'slug'], 'string', 'max' => 50],
            [['slug'], 'unique'],
            [['brandId'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brandId' => 'brandId']],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'categoryId']],
            [['photoId'], 'exist', 'skipOnError' => true, 'targetClass' => Photo::className(), 'targetAttribute' => ['photoId' => 'photoId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'productId' => Yii::t('app', 'Product ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'categoryId' => Yii::t('app', 'Category ID'),
            'brandId' => Yii::t('app', 'Brand ID'),
            'photoId' => Yii::t('app', 'Photo ID'),
            'slug' => Yii::t('app', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(Photo::className(), ['photoId' => 'photoId'])->inverseOf('photo');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['brandId' => 'brandId'])->inverseOf('products');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['categoryId' => 'categoryId'])->inverseOf('products');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto0()
    {
        return $this->hasOne(Photo::className(), ['photoId' => 'photoId'])->inverseOf('products');
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    public static function  findOneBySlug($slug)
    {
        $query = static::find();
        return $query->andWhere(['=','slug',$slug])->one();
    }

    public function generateSlug($name)
    {
        $slug = Inflector::camel2id($name);
        return $slug;
    }
}
