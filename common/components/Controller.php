<?php

namespace common\components;

use yii\web\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'language' => [
                'class' => LanguageFilter::className(),
            ],
        ];
    }
}