<?php

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\helpers\VarDumper;

class LanguageFilter extends Behavior
{
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION=> 'beforeAction',
        ];
    }

    const DEFAULT_LANGUAGE = 'en';
    const SESSION_LANGUAGE_KEY = 'language';

    public function beforeAction($event)
    {
        $language = Yii::$app->language;

        $session = Yii::$app->session;
        if($sessionLanguage = $session->get(self::SESSION_LANGUAGE_KEY)) {
            $language = $sessionLanguage;
        } else {
            $user = Yii::$app->user;
            if(!$user->getIsGuest()) {
                $language = $user->getIdentity()->getLanguage();
            }
        }

        /**
         * @todo: may be use sourceLanguage Param from main config?
         */
        if($languageQuery = Yii::$app->getRequest()->getQueryParam('l')) {
            $language = $languageQuery;
        }

        /**
         * @todo: move array with languages to params
         */
        if(in_array($language,Yii::$app->params['languageList'])) {
            Yii::$app->language = $language;
        } else {
            Yii::$app->language = self::DEFAULT_LANGUAGE;
        }

        $session->set(self::SESSION_LANGUAGE_KEY,$language);

        return $event->isValid;
    }
}