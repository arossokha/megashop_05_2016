<?php

namespace frontend\components;

use common\models\Product;
use frontend\models\Cart;
use frontend\models\CartItem;
use Yii;
use yii\base\Component;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;

class CartManager extends Component
{
    public function init()
    {
        parent::init();
        /**
         * create all that need for component works properly
         */
    }

    /**
     * @return Cart|null|static
     * @throws ServerErrorHttpException
     * @throws UnauthorizedHttpException
     * @throws \Exception
     */
    public function getCart()
    {
        if(Yii::$app->getUser()->isGuest) {
//        get cart by session id
            throw new UnauthorizedHttpException('Need login');
        } else {
//        get cart by user id
            $user = Yii::$app->user->getIdentity();
            $userId = $user->getId();
            $cart = Cart::findOne(['userId' => $userId]);
            if(null === $cart) {
                $cart = new Cart();
                $cart->userId = $userId;
                $cart->total = 0;
                if(!$cart->save()) {
                    if (YII_ENV == 'dev') {
                        throw new \Exception('Cart not saved');
                    } else {
                        throw new ServerErrorHttpException('Problem with cart. Write to administrator');
                    }
                }

            }
            return $cart;
        }

    }

    public function addToCart(Product $product,Cart $cart = null)
    {
        if(null == $cart) {
            $cart = $this->getCart();
        }

        $cartItem = CartItem::findOne(['productId' => $product->getPrimaryKey()]);
        if($cartItem) {
            throw new ServerErrorHttpException('Product is already added to other card');
            return false;
        }

        $cartItem = new CartItem();
        $cartItem->cartId = $cart->getPrimaryKey();
        $cartItem->productId = $product->getPrimaryKey();
//        $cartItem->price = $product->price;

        /**
         * @todo: validate for errors
         */
        return $cartItem->save();
    }

    public function getCount()
    {
        return 1;
    }

    public function convertCartToOrder (Cart $cart = null)
    {
        throw new \Exception('Not implemented');
    }
}