<?php

namespace frontend\components\widgets;

use Yii;
use common\models\Category;
use yii\base\Widget;
use yii\helpers\Html;

class CategoryWidget extends Widget
{
    private $_categoryList = [];

    /**
     * Class for main div block
     * @var string
     */
    public $mainDivClass = '';


    /**
     * Class for list blocks
     * @var string
     */
    public $listItemClass = '';

    /**
     * Provide settings
     */
    public function init()
    {
        parent::init();
        // get list of categories
        // provide sorting
        $this->loadCategoryList();
    }

    public function run()
    {
        $blockName = Yii::t('app', 'Categories');
        $html = '<div class="'.$this->mainDivClass.'">
        <h2>'.$blockName.'</h2>';
        $html .= '<ul>';
        foreach ($this->getCategoryList() as $category) {
            $html .= '<li class="'.$this->listItemClass.'">'
                .Html::a(Html::encode($category->name),'#')
                .'</li>';
        }
        $html .= '</ul></div>';
        return $html;
    }

    /**
     * @return array
     */
    public function getCategoryList()
    {
        return $this->_categoryList;
    }

    /**
     *
     */
    public function loadCategoryList()
    {
        $categoryList = Category::find()->all();
        $this->_categoryList = $categoryList;
    }
}