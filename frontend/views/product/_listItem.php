<?php

use Yii;
use yii\helpers\Html;
use \yii\helpers\Url;

?>

<a href="<?= Url::to(['product/view','id' => $model->productId]) ?>">

    <div class="panel panel-default">
        <div class="panel-body ">

            <h3>
                <?= $model->name; ?>
            </h3>
            <span style="color: #0000aa; font-size: 20px;">
                <?= $model->price; ?>
            </span>
            Basic panel example
            <br>
            <?= Yii::t('app','Category') ?> <?= $model->category->name; ?>
        </div>
    </div>

</a>