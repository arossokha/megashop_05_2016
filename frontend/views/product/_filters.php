<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Brand;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="product-filter">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<!--    --><?php //echo $form->field($model, 'brands')->checkboxList(
//        ArrayHelper::map(
//            Brand::find()->asArray()->all(),
//            'brandId',
//            'name'
//        )); ?>

    <?= $form->field($model, 'brands')->dropDownList(
        ArrayHelper::map(
            Brand::find()->asArray()->all(),
            'brandId',
            'name'
        ),['multiple'=> 'multiple']); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>