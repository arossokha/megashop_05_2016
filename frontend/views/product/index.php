<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use frontend\components\widgets\CategoryWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '@TODO change to Category name');
$this->params['breadcrumbs'][] = $this->title;
$this->params['meta'] = [
    [
        'name' => 'author',
        'content' => 'John Doe',
    ],
    [
        'charset' => "UTF-8"
    ],
//    [
//        'http-equiv' => "refresh",
//        'content' =>"60;url=".Yii::$app->getHomeUrl(),
//    ],

];
?>
<div class="row">
    <div class="col-sm-2">
        <?= CategoryWidget::widget([
            'listItemClass' => 'category-list-item'
        ]); ?>
    </div>
    <div class="col-sm-8">
        <div class="category-index">

            <h1><?= Html::encode($this->title) ?></h1>

            <?php
            Pjax::begin();
            $dataProvider->getModels(); // fix problem
            $pageNum = $dataProvider->getPagination()->getPage();
            $id = 'product_index_'.$pageNum;
            $dependency = [
                'class' => 'yii\caching\DbDependency',
                'sql' => 'SELECT count(1) FROM '.\common\models\Product::tableName(),
            ];

            if ($this->beginCache($id,[
                'duration' => 1800,
                'dependency' => $dependency
            ])) {
            ?>

            <?php //echo $this->render('@frontend/views/product/_search', ['model' => $searchModel]); ?>
            <?php echo $this->render('@frontend/views/product/_globalSearch', ['model' => $searchModel]); ?>

            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
//        'itemView' => function ($model, $key, $index, $widget) {
//            return Html::a(Html::encode($model->name), ['product/view', 'id' => $model->productId]);
//        },
                'itemView' => '@frontend/views/product/_listItem'
            ]); ?>

            <?php
                $this->endCache();
            }
            Pjax::end();
            ?>
        </div>

    </div>
    <div class="col-sm-2">
        <?php echo $this->render('@frontend/views/product/_filters', ['model' => $searchModel]); ?>
    </div>
</div>

