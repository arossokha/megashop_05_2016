<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<h1>Cart</h1>

<?php
/**
 * @var Cart $cart
 */

/**
 * @todo : use list widget instead of foreach
 */
echo "<ul>";
$total = 0;
foreach ($cart->cartItems as $cartItem) {
    $product = $cartItem->getProduct()->one();
    $total += $product->price;
    echo "<li>{$product->name} {$product->price}</li>";
}
echo "</ul>";

echo "<h2>Total: {$total}</h2>"

?>
