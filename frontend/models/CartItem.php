<?php

namespace frontend\models;

use Yii;
use common\models\Product;

/**
 * This is the model class for table "CartItem".
 *
 * @property integer $cartItemId
 * @property integer $cartId
 * @property integer $productId
 *
 * @property Cart $cart
 * @property Product $product
 */
class CartItem extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CartItem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cartId', 'productId'], 'integer'],
            [['cartId'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['cartId' => 'cartId']],
            [['productId'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['productId' => 'productId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cartItemId' => Yii::t('app', 'Cart Item ID'),
            'cartId' => Yii::t('app', 'Cart ID'),
            'productId' => Yii::t('app', 'Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['cartId' => 'cartId'])->inverseOf('cartItems');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['productId' => 'productId'])->inverseOf('cartItems');
    }
}
