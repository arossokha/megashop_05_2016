<?php

namespace frontend\models;

use yii\base\Model;

class SubscribeForm extends Model
{
    public $name;
    public $email;

    public function attributeLabels()
    {
        return [
            'name' => '<p>NAME</p>',
            'email' => '<b>Мыло</b>',
        ];
    }

    public function rules()
    {
        return [
            [
                ['name', 'email'], 'required', 'enableClientValidation' => false
            ],
            [
                'email', 'email', 'enableClientValidation' => false//'message' => 'Custom'
            ],
        ];
    }

    public function subscribe()
    {
        // send email
    }
}