<?php

namespace frontend\models;

use common\models\User;

use Yii;

/**
 * This is the model class for table "Cart".
 *
 * @property integer $cartId
 * @property integer $userId
 * @property string $total
 *
 * @property User $user
 * @property CartItem[] $cartItems
 */
class Cart extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'integer'],
            [['total'], 'number'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cartId' => Yii::t('app', 'Cart ID'),
            'userId' => Yii::t('app', 'User ID'),
            'total' => Yii::t('app', 'Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId'])->inverseOf('carts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartItems()
    {
        return $this->hasMany(CartItem::className(), ['cartId' => 'cartId'])->inverseOf('cart');
    }
}
