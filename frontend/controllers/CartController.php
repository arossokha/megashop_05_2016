<?php

namespace frontend\controllers;

use common\models\Product;
use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;

class CartController extends \yii\web\Controller
{
    public function actionIndex()
    {
        /**
         * @var Cart $cart
         */
        $cart = Yii::$app->get('cart')->getCart();
        return $this->render('index',[
            'cart' => $cart
        ]);
    }

    public function actionBuy($productId)
    {
        $product = Product::findOne($productId);
        if(!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        if(!Yii::$app->get('cart')->addToCart($product)) {
            throw new \LogicException('Product not added to cart');
        }

        $this->redirect(['cart/index']);
    }

    public function actionToOrder()
    {
        return $this->render('to-order');
    }

}
