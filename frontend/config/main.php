<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$rules = require ('rules.php');

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            /**
             * @todo : change to false for google or find not to deal with redirect_uri problem
             * @todo : set in redirect_uri  """http://megashop.dev/site/login?service=google""" with service param
             */
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
                'google' => [
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                    'clientId' => '824882006401-cpqirsuvha8fb3uninkc5r1bfvphjsvc.apps.googleusercontent.com',
                    'clientSecret' => '8mePmMMIQr2TfcoSNSTRwRIA',
                    'title' => 'Google',
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $rules
        ],
        'cart' => [
            'class' => 'frontend\components\CartManager'
        ],
        'i18n' => [
                'translations' => [
                    'eauth' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@eauth/messages',
                    ],
            ],
        ],

    ],
    'params' => $params,
];
