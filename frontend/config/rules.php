<?php

return [
//    'login/<service:google>' => 'site/login',
    'site/<action>' => 'site/<action>',
    'product/<id:\d+>' => 'product/view',
    'product/category-<categorySlug:[\w\-]+>' => 'product/index',
    'product/<slug:[\w\-]+>' => 'product/slug',
    /**
     * @todo : add new rule
     */
//    '<categorySlug:[\w\-]+>/<slug:[\w\-]+>' => 'product/categorySlug',
];